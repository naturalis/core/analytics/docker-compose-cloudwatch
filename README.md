# docker-compose-cloudwatch

## Introduction

We want to monitor AWS with our standard tools; Prometheus, Grafana,
alert-dashboard. This docker-compose setup exports Cloudwatch metrics to be
scraped by Prometheus.

## Installation

There are 2 projects: we will use [this
one](https://github.com/prometheus/cloudwatch_exporter) and [not this
one](https://github.com/nerdswords/yet-another-cloudwatch-exporter), because it
still developed.

The scraper can be intalled as an simple Java process, which is handy for
debuging. We used this to get the credentials correct.

### Credentials

The AWS-credential policy we used for _analytics-cloudwatch_ is: 

```yaml
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "cloudwatch:Describe*",
                "cloudwatch:Get*",
                "cloudwatch:List*",
                "tag:GetResources"
            ],
            "Effect": "Allow",
            "Resource": "*"
        }
    ]
}
```

We added the _tag:GetResources_ to be able to read labels.
We then created a key and stored this in HC Vault

### Container

We pinned the version of the container. For the config we simplified the
[EC2-example](https://github.com/prometheus/cloudwatch_exporter/blob/master/examples/EC2.yml).
There is
[link](https://github.com/djloude/cloudwatch_exporter_metrics_config_builder)
to 'a configuration builder', but I didn't work for me.

After starting port 9106 is available and can be queried like this:

```bash
curl http://127.0.0.1:9106/metrics
```

Because we will run this on the (AWS-) prometheus nothing else (like Traefik or
so) is needed.

### Issue

The alert wasn't firing when it should; this was a [timing
issue](https://github.com/prometheus/cloudwatch_exporter/issues/115) and fixed
by setting `set_timestamp: false` in the cloudscrape-config (globally).
